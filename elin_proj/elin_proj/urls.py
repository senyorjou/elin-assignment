from django.conf.urls import patterns, include, url
# from django.views.generic import TemplateView

from django.conf import settings

from django.contrib import admin
admin.autodiscover()

from tastypie.api import Api
from assig.api import ItemResource

v1_api = Api(api_name='v1')
v1_api.register(ItemResource())

urlpatterns = patterns('',
    # url(r'^$', TemplateView.as_view(template_name='base.html')),
    url(r'^$', 'assig.views.index', name='index'),
    url(r'^load_file$', 'assig.views.load_file', name='load_file'),
	url(r'^api/', include(v1_api.urls)),
    url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG == True:
    urlpatterns += patterns('', 
        (r'^media/images/(?P<path>.*)$',
         'django.views.static.serve',
         {'document_root': settings.MEDIA_ROOT + '/images/'}),

        (r'^media/ads/(?P<path>.*)$',
         'django.views.static.serve',
         {'document_root': settings.MEDIA_ROOT + '/ads/'}),

        (r'^media/CACHE/(?P<path>.*)$',
         'django.views.static.serve',
         {'document_root': settings.MEDIA_ROOT + '/CACHE/'})
    )