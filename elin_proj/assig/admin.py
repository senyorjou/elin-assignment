from django.contrib import admin

# from django.conf import settings
# from imagekit.admin import AdminThumbnail

from assig.models import Item


class ItemAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'url')


admin.site.register(Item, ItemAdmin)