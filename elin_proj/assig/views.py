# Create your views here.
import os
import csv
import requests

from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _

from django.conf import settings

from assig.models import Item


def index(request):
    return render_to_response('index.html', {},
                              context_instance=RequestContext(request))

def load_file(request):
    file_path = os.path.join(settings.MEDIA_ROOT, 'test_application.csv')

    with open(file_path) as f:
        has_header = csv.Sniffer().has_header(f.readline())
        if has_header:
            first_row = 1
            title, desc, url = [0, 1, 2]
        else:
            # We have to determine the order of fields
            first_row = 0
            title, desc, url = [2, 0, 1]

        
        f.seek(0)
        content = list(csv.reader(f, delimiter=','))
        total_objects_created = images_added= 0
        for row in content[first_row:]:
            obj, created = Item.objects.get_or_create(
                title=row[title], 
                defaults = {'description':row[desc], 'url':row[url]})

            if created:
                total_objects_created += 1
                # determine if url is an image
                # ask just for http header
                if row[url]:
                    req = requests.head(row[url])
                    if req.status_code == 200 and 'image/' in req.headers.get('content-type'):
                        obj.create_image_from_url()
                        images_added += 1

    messages.info(request, _('%s objects created. %d images added.')\
                           % (total_objects_created, images_added))
    
    return redirect('index')