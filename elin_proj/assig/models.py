import requests
import hashlib
from PIL import Image

from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
from django.template.defaultfilters import slugify

from django.db import models
from django.utils.translation import ugettext_lazy as _

from imagekit import ImageSpec, register
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFit


class Item(models.Model):
    title = models.CharField(_('Title'), max_length=250)
    description = models.CharField(_('Description'), max_length=250, blank=True,
                                   default=True)
    url = models.URLField(_('URL'), max_length=512, blank=True,  default='')
    image = models.ImageField(_('Image'), upload_to='images', max_length=250, 
                              default='')

    image_100 = ImageSpecField(source='image',
                               processors=[ResizeToFit(100, 75)],
                               format='JPEG',
                               options={'quality': 100})
    image_480 = ImageSpecField(source='image',
                               processors=[ResizeToFit(480, 380)],
                               format='JPEG',
                               options={'quality': 70})

    #
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


    class Meta:
        db_table = u'assig_item'
        ordering = ['title']

    def __unicode__(self):
        return u'%s' % self.title

    def create_image_from_url(self):
        r = requests.get(self.url)

        img_temp = NamedTemporaryFile(delete=True)
        img_temp.write(r.content)
        img_temp.flush()

        try:
            img = Image.open(img_temp.name)
            file_name = '%s-%s%.s' % (slugify(self.title), 
                                 hashlib.md5(str(self.id)).hexdigest()[:5],
                                 img.format.lower())
            self.image.save(file_name, File(img_temp), save=True)
        
        except IOError:
            # Not an image?
            return



class Image480(ImageSpec):
    processors = [ResizeToFit(480, 295)]
    format = 'JPEG'
    options = {'quality': 70}

register.generator('assig:image480', Image480)


class Image100(ImageSpec):
    processors = [ResizeToFit(100, 75)]
    format = 'JPEG'
    options = {'quality': 100}

register.generator('assig:image100', Image100)

