from django.template.defaultfilters import truncatechars

from tastypie.resources import ModelResource
from assig.models import Item


class ItemResource(ModelResource):
    class Meta:
        queryset = Item.objects.all()
        resource_name = 'item'
        excludes = ['created_at', 'updated_at', 'url', 'image']

    def dehydrate(self, bundle):
        if bundle.obj.image:
            bundle.data['image_full'] = bundle.obj.image.url
            bundle.data['image_100'] = bundle.obj.image_100.url
            bundle.data['image_480'] = bundle.obj.image_480.url
                        
        bundle.data['smart_title'] = truncatechars(bundle.obj.title, 30)
        
        return bundle